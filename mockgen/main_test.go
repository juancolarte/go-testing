package main

import (
	"fmt"
	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"somestore/mock_service"
	"somestore/service"
	"testing"
)

func TestAddone(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockedService := mock_service.NewMockStoreService(mockCtrl)
	newitem := service.Item{Key: "01", Value: "some value"}
	mockedService.EXPECT().Add("01", "some value").Return(newitem) //.Times(1)
	s = mockedService
	createdItem := addone("01", "some value")
	fmt.Println(createdItem.Key)
	assert.Equal(t, createdItem, newitem, "Data structure created correctly")
}
