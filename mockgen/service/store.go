package service

type Item struct {
	Key   string
	Value string
}

var data []Item

type store struct{}

// GreeterService is service to greet your friends.
type StoreService interface {
	Add(key string, value string) Item
	GetOne(key string) Item
	GetAll() []Item
}

func (s store) Add(key string, value string) Item {
	newitem := Item{Key: key, Value: value}
	data = append(data, newitem)
	return newitem
}

func (s store) GetOne(key string) Item {
	var answer Item
	for _, searchedItem := range data {
		if searchedItem.Key == key {
			answer = searchedItem
		}
	}
	return answer
}

func (s store) GetAll() []Item {
	return data
}

func NewStore() StoreService {
	return &store{}
}
