package main

import (
	"fmt"
	"somestore/service"
)

var s service.StoreService

func addone(key string, value string) service.Item {
	createdItem := s.Add(key, value)
	return createdItem
}

func bringem() []service.Item {
	return s.GetAll()
}

func main() {

	s = service.NewStore()
	item1 := addone("first", "value one")
	item2 := service.Item{Key: "juan", Value: "some stuff"}
	addone("second", "value two")
	addone("third", "third")
	fmt.Println("item : ")
	fmt.Printf("%+v ", item1)
	fmt.Println("::::::: ")
	fmt.Println("item2 : ")
	fmt.Printf("%+v ", item2)
	fmt.Println("::::::: ")

	items := bringem()
	fmt.Println(items)
	//fmt.Println(item)
}
