package main

import (
	"errors"
	"somestore/mocks"
	"somestore/service"
	"testing"

	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
)

func Test_app_Addone(t *testing.T) {

	StoreMock := mocks.StoreService{}
	item1 := service.Item{"01", "value 01"}
	StoreMock.On("Add", item1.Key, item1.Value).Return(item1, nil)
	item2 := service.Item{"", ""}
	FailStoreMock := mocks.StoreService{}
	FailStoreMock.On("Add", mock.Anything, mock.Anything).Return(item2, errors.New("Invalid Argument"))

	type fields struct {
		store mocks.StoreService
	}
	type args struct {
		key   string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    service.Item
		wantErr bool
	}{
		{"success", fields{StoreMock}, args{item1.Key, item1.Value}, item1, false},
		{"should fail", fields{FailStoreMock}, args{item2.Key, item2.Value}, item2, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := app{tt.fields.store}
			got, err := a.Addone(tt.args.key, tt.args.value)
			assert.Equal(t, (err != nil), tt.wantErr, "Data structure created correctly")
			assert.Equal(t, got, tt.want, "Data structure created correctly")
		})
	}
}
