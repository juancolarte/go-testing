package service

import "errors"

type Item struct {
  Key string
  Value string
}
var data []Item

type store struct {}

type StoreService interface {
	Add(key string, value string) (Item , error)
    GetOne(key string) (Item, error)
    GetAll()  ([]Item, error)
}

func (s store) Add(key string, value string) (Item , error) {
    
	if key == "" || value == "" {
		return Item{}, errors.New("Invalid Argument")
	}

    newitem := Item{Key: key, Value: value}
    data = append(data , newitem)
	return newitem, nil
}

func (s store) GetOne(key string) (Item, error) {
    var answer Item
    for _, searchedItem := range data {
      if searchedItem.Key == key {
        answer = searchedItem
      }
    }    
  return answer, nil
}

func (s store) GetAll() ([]Item, error) {
    return data, nil
}

func NewStore() StoreService {
    return  &store{}
}