package main

import (
        "fmt"
        "somestore/service"
)

func main(){
        s := service.NewStore()
        App := NewApp(s)
        App.Addone("first", " Primero 1")
        App.Addone("second", " Segundo 2")
        App.Addone("third", " Tercero 3")

        item, error := App.Addone("", "sot")
        if error != nil {
          fmt.Println("Previous Line Failed")
          fmt.Println("item : ", item)
        }

        items, _ := App.Bringem()
        for _, i := range items {
          fmt.Println(i)
        }    
}

