# Go Testing

Golang testing , using interface mocking , table driven unit tests, mock generators (mockgen , mockery)

NOTES:
go fmt ./...

USE mockery:

cd mockery
go test

USE mockgen:
cd mockgen
go test

```
.
├── mockery
│   ├── app.go
│   ├── app_test.go
│   ├── go.mod
│   ├── go.sum
│   ├── main.go
│   ├── mocks
│   │   └── StoreService.go
│   ├── README.md
│   ├── run.sh
│   └── service
│       └── store.go
├── mockgen
│   ├── go.mod
│   ├── go.sum
│   ├── main.go
│   ├── main_test.go
│   ├── mock_service
│   │   └── mocked_store.go
│   ├── README.md
│   ├── run.sh
│   └── service
│       └── store.go
└── README.md

6 directories, 18 files
```
