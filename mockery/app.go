package main

import (
	"somestore/service"
)

type app struct {
	store service.StoreService
}

func (a app) Addone(key string, value string) (service.Item, error) {
	createdItem, error := a.store.Add(key, value)
	return createdItem, error
}

func (a app) Bringem() ([]service.Item, error) {
	all, err := a.store.GetAll()
	return all, err
}

func NewApp(servicestore service.StoreService) app {
	return app{store: servicestore}
}
