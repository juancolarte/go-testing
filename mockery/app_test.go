package main

import (
	"errors"
	"somestore/mocks"
	"somestore/service"
	"testing"

	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
)

func Test_app_Addone(t *testing.T) {

	StoreMock := mocks.StoreService{}
	item1 := service.Item{Key: "01", Value: "value 01"}
	StoreMock.On("Add", item1.Key, item1.Value).Return(item1, nil)

	FailStoreMock := mocks.StoreService{}
	item2 := service.Item{Key: "", Value: ""}
	arg_error := errors.New("Invalid Argument")
	FailStoreMock.On("Add", mock.Anything, mock.Anything).Return(item2, arg_error)

	type fields struct {
		store mocks.StoreService
	}
	type args struct {
		key   string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    service.Item
		wantErr bool
	}{
		// TEST Cases:
		{
			name:   "success",
			fields: fields{store: StoreMock},
			args: args{key: item1.Key,
				value: item1.Value},
			want:    item1,
			wantErr: false,
		},
		{
			name:   "should fail",
			fields: fields{store: FailStoreMock},
			args: args{key: item2.Key,
				value: item2.Value},
			want:    item2,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := app{
				store: tt.fields.store,
			}
			got, err := a.Addone(tt.args.key, tt.args.value)
			assert.Equal(t, (err != nil), tt.wantErr, "Data structure created correctly")
			assert.Equal(t, got, tt.want, "Data structure created correctly")
		})
	}
}
